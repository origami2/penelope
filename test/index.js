var assert = require('assert');
var Penelope = require('..');
var ConnectedEmitters = require('origami-connected-emitters');

describe('Penelope', function () {
  it('exports a function', function () {
    assert.equal('function', typeof(Penelope));
  });
  
  describe(
    '.addHandler', 
    function () {
      it('requires a kind', function () {
        assert.throws(
          function () {
            var bus = new ConnectedEmitters();
            var target = new Penelope(bus.createEmitter());
            
            target.addHandler();
          },
          /kind is required/
        );
      });
      
      it('requires a handler', function () {
        assert.throws(
          function () {
            var bus = new ConnectedEmitters();
            var target = new Penelope(bus.createEmitter());
            
            target.addHandler('my-kind1');
          },
          /handler is required/
        );
      });
      
      it('accepts handler', function () {
            var bus = new ConnectedEmitters();
            var target = new Penelope(bus.createEmitter());
        
        target.addHandler('my-kind1', function () {});
      });
      
      it('invokes handler for known open-subsocket kind', function (done) {
        var bus = new ConnectedEmitters();
        var mainSocket = bus.createEmitter();
        var target = new Penelope(mainSocket);
        
        target
        .addHandler(
          'my-kind1', 
          function (socket, params) {
            try {
              assert(socket);
              assert.deepEqual({param1: true}, params);
              
              done();
            } catch (e) {
              done(e);
            }
            
            return Promise.resolve();
          }
        );
        
        bus.createEmitter().emit('open-subsocket', 'abcd1234', 'my-kind1', { param1: true }, function () {});
      });
      
      it('invokes open-subsocket callback with handler error', function (done) {
        var bus = new ConnectedEmitters();
        var mainSocket = bus.createEmitter();
        var target = new Penelope(mainSocket);
        
        target
        .addHandler(
          'my-kind1', 
          function (socket, params) {
            throw new Error('my error');
          }
        );
        
        bus
        .createEmitter()
        .emit(
          'open-subsocket',
          'abcd1234',
          'my-kind1',
          { param1: true },
          function (err) {
            try {
              assert.equal('my error', err);
              
              done();
            } catch (e) {
              done(e);
            }
          }
        );
      });
      
      it('waits for promise resolution to invoke open-subsocket callback', function (done) {
        var bus = new ConnectedEmitters();
        var mainSocket = bus.createEmitter();
        var target = new Penelope(mainSocket);
        var resolved = true;
        
        target
        .addHandler(
          'my-kind1', 
          function (socket, params) {
            return new Promise(function (resolve, reject) {
              resolved = true;
              
              resolve();
            });
          }
        );
        
        bus
        .createEmitter()
        .emit(
          'open-subsocket',
          'abcd1234',
          'my-kind1',
          { param1: true },
          function (err) {
            try {
              assert(!err);
              assert(resolved);
              
              done();
            } catch (e) {
              done(e);
            }
          }
        );
      });
      
      it('waits for promise rejection to invoke open-subsocket callback with error', function (done) {
        var bus = new ConnectedEmitters();
        var mainSocket = bus.createEmitter();
        var target = new Penelope(mainSocket);
        var rejected = true;
        
        target
        .addHandler(
          'my-kind1', 
          function (socket, params) {
            return new Promise(function (resolve, reject) {
              rejected = true;
              
              reject('my error');
            });
          }
        );
        
        bus
        .createEmitter()
        .emit(
          'open-subsocket',
          'abcd1234',
          'my-kind1',
          { param1: true },
          function (err) {
            try {
              assert.equal('my error', err);
              assert(rejected);
              
              done();
            } catch (e) {
              done(e);
            }
          }
        );
      });
    }
  );
  
  describe('.openSubsocket', function () {
    it('requires a socket', function () {
      assert.throws(
        function () {
          new Penelope();
        },
        /socket is required/
      );
    });
    
    it('requires an initializer', function () {
      assert.throws(
        function () {
          var bus = new ConnectedEmitters();
          var target = new Penelope(bus.createEmitter());
          
          target.openSubsocket();
        },
        /initializer is required/
      );
    });
    
    it('emits open-socket in main socket', function (done) {
      var bus = new ConnectedEmitters();
      var mainSocket = bus.createEmitter();
      var target = new Penelope(mainSocket);
      
      bus
      .createEmitter()
      .on(
        'open-subsocket', 
        function (id, kind, params, callback) {
          try {
            assert(id);
            assert.equal('string', typeof(id));
            assert.equal('my-kind1', kind);
            assert.deepEqual({ param1: true }, params);
            assert.equal('function', typeof(callback));
            
            done();
          } catch (e) {
            done(e);
          }
        }
      );

      target
      .openSubsocket(
        function () {
          return Promise.resolve();
        },
        'my-kind1',
        { param1: true }
      );
    });
    
    it('invokes initializer with a socket', function (done) {
      var bus = new ConnectedEmitters();
      var target = new Penelope(bus.createEmitter());
      var initializer = function (socket) {
        try {
          assert(socket);
          
          done();
        } catch (e) {
          done(e);
        }
      };
      
      target.openSubsocket(initializer);
    });
    
    it('returns a promise', function () {
      var bus = new ConnectedEmitters();
      var target = new Penelope(bus.createEmitter());
      
      var result = target.openSubsocket(function () {});
      
      assert(result instanceof Promise);
    });
    
    it('rejects promise if open-subsocket callback is invoked with error', function (done) {
      var bus = new ConnectedEmitters();
      var mainSocket = bus.createEmitter();
      var target = new Penelope(mainSocket);
      
      bus
      .createEmitter()
      .on(
        'open-subsocket', 
        function (id, kind, params, callback) {
          callback('my error');
        }
      );

      var promise = target.openSubsocket(function () {
        return Promise.resolve();
      });
      
      promise
      .catch(function (err) {
        try {
          assert.equal('my error', err);
          
          done();
        } catch (e) {
          done(e);
        }
      });
    });
    
    it('retains outgoing messages until open-subsocket callback is invoked', function (done) {
      var bus = new ConnectedEmitters();
      var socket1 = bus.createEmitter();
      var socket2 = bus.createEmitter();
      var target = new Penelope(socket1);
      var ready = false;
      
      socket2
      .on(
        'subsocket-message', 
        function (id, eventName, message, callback) {
          try {
            assert(ready);
            assert.equal('string', typeof(id));
            assert.equal('hello', eventName);
            assert.deepEqual(['there'], message);
            assert.equal('function', typeof(callback));
            
            done();
          } catch (e) {
            done(e);
          }
        }
      );

      socket2
      .on(
        'open-subsocket', 
        function (id, kind, params, callback) {
          callback();
        }
      );

      var resolvePromise;
      
      target.openSubsocket(function (socket) {
        socket.emit('hello', 'there', function () {});
        
        return new Promise(function (resolve, reject) {
          resolvePromise = function () {
            ready = true;
            resolve();
          };
        });
      });
      
      resolvePromise();
    });
    
    it('retains incoming messages until open-subsocket callback is invoked', function (done) {
      var bus = new ConnectedEmitters();
      var socket1 = bus.createEmitter();
      var socket2 = bus.createEmitter();
      var target = new Penelope(socket1);
      var ready = false;

      socket2
      .on(
        'open-subsocket', 
        function (id, kind, params, callback) {
          socket2
          .emit(
            'subsocket-message',
            id,
            'hello',
            ['there'],
            function () {
            }
          );
          
          ready = true;
          callback();
        }
      );

      var resolvePromise;
      
      target
      .openSubsocket(function (socket) {
        socket
        .on(
          'hello',
          function (phrase, callback) {
            try {
              assert(ready);
              assert.equal('there', phrase);
              assert.equal('function', typeof(callback));
              
              done();
            } catch (e) {
              done(e);
            }
          }
        );
        
        return new Promise(function (resolve, reject) {
          resolvePromise = resolve;
        });
      });
      
      resolvePromise();
    });
    
    it('rejects open-subsocket on unknown kind', function (done) {
      var bus = new ConnectedEmitters();
      var socket1 = bus.createEmitter();
      var socket2 = bus.createEmitter();
      new Penelope(socket1);

      socket2
      .emit(
        'open-subsocket', 
        'abcd1234',
        'unknown-kind',
        {},
        function (err) {
          try {
            assert.equal('unknown kind', err);
            
            done();
          } catch (e) {
            done(e);
          }
        }
      );
    });
  })
});