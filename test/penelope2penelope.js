var assert = require('assert');
var Penelope = require('..');
var ConnectedEmitters = require('origami-connected-emitters');

describe('Penelope2Penelope', function () {
  it('works on both ends', function (done) {
    var bus = new ConnectedEmitters();
    var socket1 = bus.createEmitter();
    var socket2 = bus.createEmitter();
    var penelope1 = new Penelope(socket1);
    var penelope2 = new Penelope(socket2);
    
    penelope1
    .addHandler(
      'kind1', 
      function (socket, params) {
        socket.on('hello', function () {
          done();
        });
        
        return Promise.resolve();
      }
    );
    
    penelope2
    .openSubsocket(
      function (subsocket) {
        subsocket.emit('hello');
        
        return Promise.resolve();
      },
      'kind1',
      {}
    );
  });
  
  it('passes open parameters', function (done) {
    var bus = new ConnectedEmitters();
    var socket1 = bus.createEmitter();
    var socket2 = bus.createEmitter();
    var penelope1 = new Penelope(socket1);
    var penelope2 = new Penelope(socket2);
    
    penelope1
    .addHandler(
      'kind1', 
      function (socket, params) {
        try {
          assert.deepEqual({param1: true}, params);
          
          done();
        } catch (e) {
          done(e);
        }
      }
    );
    
    penelope2
    .openSubsocket(
      function () {
        return Promise.resolve();
      },
      'kind1',
      { param1: true}
    );
  });
  
  it('passes other end open error', function (done) {
    var bus = new ConnectedEmitters();
    var socket1 = bus.createEmitter();
    var socket2 = bus.createEmitter();
    var penelope1 = new Penelope(socket1);
    var penelope2 = new Penelope(socket2);
    
    penelope1
    .addHandler(
      'kind1', 
      function (socket, params) {
        throw new Error('my error');
      }
    );
    
    penelope2
    .openSubsocket(
      function (subsocket) {
        return Promise.resolve();
      },
      'kind1',
      {}
    )
    .catch(function (err) {
      try {
        assert.equal('my error', err);
        
        done();
      } catch (e) {
        done(e);
      }
    });
  });
});